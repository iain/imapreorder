// Copyright © 2014 Iain Nicol
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.iainnicol.imapreorder

import com.sun.mail.imap.{AppendUID, IMAPFolder, IMAPMessage, IMAPStore}
import javax.mail._
import scala.collection.immutable.Seq
import resource.{ManagedResource, makeManagedResource}

/** General purpose IMAP functions.  A lot of functions are simple
  * wrappers round javamail functions, allowing code with better type
  * safety to be written.
  */
object Imap {

  object Protocol extends Enumeration {
    type Protocol = Value
    val imap, imaps = Value
  }

  /** Copies the messages.  Preserves their INTERNALDATEs.
    *
    * @param source source folder; must be open.
    * @param dest destination folder; must be open.
    * @return the copies.
    */
  def copy(
    messages: Seq[IMAPMessage],
    source: IMAPFolder,
    dest: IMAPFolder): Seq[IMAPMessage] = {
    val copyUids: Seq[AppendUID] =
      source.copyUIDMessages(messages.toArray, dest).toVector
    assert(copyUids != null)
    assert(copyUids.length == messages.length)
    val destUidValidity: Long = dest.getUIDValidity
    for (copyUid <- copyUids) {
      assert(copyUid != null)
      assert(copyUid.uidvalidity == destUidValidity)
    }

    var msgsCopy: Seq[IMAPMessage] = getMessagesByUid(
      dest,
      copyUids.map(_.uid).toArray)
    // Sometimes we have to try a second time.  WTF.
    if (msgsCopy.exists(_ == null)) {
      msgsCopy = getMessagesByUid(
        dest,
        copyUids.map(_.uid).toArray)
    }
    assert(msgsCopy.forall(_ != null))
    return msgsCopy
  }

  /** Sets the deleted flag on the messages.
    *
    * @param folder folder containing the messages.
    */
  def deleteMessages(
    folder: IMAPFolder,
    messages: Iterable[IMAPMessage]): Unit = {
    folder.setFlags(
      messages.toArray: Array[Message],
      new Flags(Flags.Flag.DELETED), true)
  }

  /** Fetches metadata about the messages, in one batch.  Note that the
    * input messages will be mutated to contain said data.
    */
  def fetchMetadata(
    messages: Iterable[IMAPMessage],
    folder: IMAPFolder): Seq[IMAPMessage] = {
    var fetchProfile = new FetchProfile();
    // ENVELOPE also causes INTERNALDATE to be fetched:
    fetchProfile.add(FetchProfile.Item.ENVELOPE);
    fetchProfile.add(UIDFolder.FetchProfileItem.UID)
    val myMessages: Seq[IMAPMessage] = messages.toVector
    folder.fetch(myMessages.toArray, fetchProfile);
    myMessages
  }

  /** Gets all the messages in the folder.
    */
  def getMessages(folder: IMAPFolder): Seq[IMAPMessage] = {
    folder.getMessages.map(_.asInstanceOf[IMAPMessage]).toVector
  }

  /** Gets the messages from the folder for the given UIDs.
    */
  def getMessagesByUid(
    folder: IMAPFolder,
    uids: Iterable[Long]): Seq[IMAPMessage] = {
    folder.getMessagesByUID(uids.toArray)
      .map(_.asInstanceOf[IMAPMessage])
      .toVector
  }

  /** Returns an open folder with the given name.  This is a resource is
    * usable with the scala-arm library.  Upon release, closes the
    * directory.
    *
    * @param store store which directly contains the folder.
    * @param folderName name of the folder.
    * @param mode used when opening the folder.
    */
  def getFolder(
    store: IMAPStore,
    folderName: String,
    mode: Int) : ManagedResource[IMAPFolder] = {
    val folder: IMAPFolder =
      store.getFolder(folderName).asInstanceOf[IMAPFolder]
    folder.open(mode)
    val resource = makeManagedResource(folder)({ folder =>
      folder.close(false /* Expunge flag */) })(List())
    resource
  }

  /** Returns a store for the IMAP protocol.
    */
  def getImapStore(session: Session): IMAPStore = {
    session.getStore("imap").asInstanceOf[IMAPStore]
  }

  /* Returns a store for IMAPS, that is IMAP over SSL or TLS.
   */
  def getImapsStore(session: Session): IMAPStore = {
    session.getStore("imaps").asInstanceOf[IMAPStore]
  }

  /* Returns a store for the protocol. */
  def getStore(session: Session, protocol: Protocol.Value): IMAPStore = {
    protocol match {
      case Protocol.imap => getImapStore(session)
      case Protocol.imaps => getImapsStore(session)
    }
  }
}
