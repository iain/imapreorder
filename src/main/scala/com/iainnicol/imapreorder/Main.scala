// Copyright © 2014 Iain Nicol
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.iainnicol.imapreorder

import javax.mail.Session
import org.rogach.scallop
import resource.managed

/** Contains the program entry point.
  */
object Main {

  /** Program entry point.
    */
  def main(args: Array[String]): Unit = {
    val conf = new Conf(args)
    val protocol =
      if (conf.imaps()) Imap.Protocol.imaps else Imap.Protocol.imap;
    main(
      protocol,
      conf.host(),
      conf.port(),
      conf.username(),
      conf.password(),
      conf.folder(),
      conf.expunge())
  }

  private def main(
        protocol: Imap.Protocol.Value,
        host: String, port: Int,
        username: String, password: String,
        folder: String,
        expunge: Boolean): Unit = {
    println("Please be patient.")
    val session = Session.getDefaultInstance(System.getProperties, null)
    for (store <- managed(Imap.getStore(session, protocol))) {
      store.connect(host, port, username, password)
      Reorder.byInternalDate(store, folder, expunge);
    }
    println("Done.")
  }
}

private class Conf(arguments: Seq[String])
    extends scallop.ScallopConf(arguments) {
  banner(
    """
      |Reorders the messages in an IMAP folder so that sorting them by
      |UID also sorts them by received date.  Works by copying the
      |messages into the correct order, then deleting the unordered
      |originals.
    """.trim.stripMargin.replace('\n', ' ') + "\n" +

    "\n" +

    """
      |Usage: imapreorder --host HOST --username USER --password PASS
      |--folder FOLDER [OPTIONS]
    """.trim.stripMargin.replace('\n', ' ') + "\n" +

    """
      |Options:
    """.trim.stripMargin.replace('\n', ' '))

  version(
    """imapreorder 1.0.0
      |Copyright © 2014 Iain Nicol""".stripMargin)

  val imaps = opt[Boolean](
    argName = "imaps",
    descr = "Use IMAPS (port 993)",
    noshort = true)
  val host = opt[String](
    argName = "host",
    required = true,
    descr = "Host; required")
  val port = opt[Int](
    argName = "port",
    descr = "Port",
    noshort = true)
  val username = opt[String](
    argName = "username",
    required = true,
    descr = "Username; required")
  val password = opt[String](
    argName = "password",
    required = true,
    descr = "Password; required")
  val folder = opt[String](
    argName = "folder",
    required = true,
    descr = "Email folder; required")
  val expunge = opt[Boolean](
    argName = "expunge",
    descr = "Expunge unordered messages upon success, instead of simply " +
      "setting the deleted flag",
    noshort = true)
}
