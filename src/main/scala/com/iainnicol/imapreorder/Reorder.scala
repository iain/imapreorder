// Copyright © 2014 Iain Nicol
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.iainnicol.imapreorder

import com.sun.mail.imap.{IMAPMessage, IMAPFolder, IMAPStore}
import javax.mail.Folder
import scala.collection.immutable.Seq
import scala.collection.mutable.ListBuffer
import scala.reflect.ClassTag

/** Main program logic.
 */
object Reorder {

  /** Changes the UIDs of some of the messages in the folder.  Afterwards,
    * if sorting all of the folder's messages increasing by UID, the
    * messages will also be weakly monotonically increasing by
    * INTERNALDATE.
    *
    * The change is made by copying messages to and from a temporary
    * folder.  It is designed to work safely.  If interrupted, the worst
    * that can happen is that duplicate messages end up in the folder,
    * or the temporary folder may not be removed.
    *
    * Upon success, deletes the now-copied messages.
    *
    * @param store an open store.
    * @param expunge whether to expunge the folder upon success.
    */
  def byInternalDate(
    store: IMAPStore,
    folderName: String,
    expunge: Boolean): Unit =
  {
    for (
      origFolder <- Imap.getFolder(store, folderName, Folder.READ_WRITE);
      tmpFolder <- Tmp.ImapFolder(origFolder))
    {
      var origMsgsAllUnsorted: Seq[IMAPMessage] = Imap.getMessages(origFolder)
      origMsgsAllUnsorted = chunkApplyAndCombine(
        origMsgsAllUnsorted,
        ((msgsChunk: Iterable[IMAPMessage]) =>
          Imap.fetchMetadata(msgsChunk, origFolder)))
      val origMsgsToMove: Seq[IMAPMessage] =
        getBadlyOrderedMsgs(origMsgsAllUnsorted, origFolder)

      var tmpMsgs: Seq[IMAPMessage] = chunkApplyAndCombine(
        origMsgsToMove,
        ((msgsChunk: Iterable[IMAPMessage]) =>
          Imap.copy(msgsChunk.toVector, origFolder, tmpFolder)))
      tmpMsgs = chunkApplyAndCombine(
         tmpMsgs,
         ((msgsChunk: Iterable[IMAPMessage]) =>
           Imap.fetchMetadata(msgsChunk, tmpFolder)))

       val tmpMsgsSorted: Seq[IMAPMessage] =
         tmpMsgs.sortBy(m => m.getReceivedDate()).toVector
       chunkApplyAndCombine(
         tmpMsgsSorted,
         ((msgsChunk: Iterable[IMAPMessage]) =>
           Imap.copy(msgsChunk.toVector, tmpFolder, origFolder)),
         1)

       chunkApplyAndCombine(
         origMsgsToMove,
         { (msgsChunk: Iterable[IMAPMessage]) =>
           val msgsChunkVec: Seq[IMAPMessage] = msgsChunk.toVector
           Imap.deleteMessages(origFolder, msgsChunkVec);
           msgsChunkVec.map(_ => ()) })
      if (expunge)
        origFolder.expunge
    }
  }

  /** Groups the iterable into chunks, applies an action to each chunk,
    * then combines the results.
    *
    * Chunking is useful because, for example, IMAP servers tend to have
    * a limit to the number of messages supported by individual IMAP
    * requests.  At the same time, chunking multiple messages into one
    * request performs better than individual requests for each message.
    */
  private def chunkApplyAndCombine[T, U: ClassTag](
    iter: Iterable[T],
    actionOnChunk: Iterable[T] => Iterable[U],
    chunkSize: Int = 1000
  ): Seq[U] = {
    val chunks: Iterator[Iterable[T]] = iter.grouped(chunkSize)
    val combined: ListBuffer[U] = ListBuffer()
    for (chunk <- chunks) {
      combined ++= actionOnChunk(chunk)
    }
    combined.toVector
  }

  /** Returns the messages in the folder which need reordered.
    *
    * A message needs reordered if its sort order by increasing UID
    * disagrees with the sort order by increasing INTERNALDATE.
    *
    * All successive messages after a message needing reordered will
    * also need reordered.  The reason why is we can increase the UID of
    * the first badly ordered message by copying the message to a
    * temporary folder then back into the folder.  The copy will then
    * have a UID higher than all other messages in the folder.  This
    * causes the messages in between the original and the copy of the
    * first badly ordered message to be out of order.
    *
    * @param messages all of the messages in the folder.
    */
  private def getBadlyOrderedMsgs(
    messages: Seq[IMAPMessage],
    folder: IMAPFolder): Seq[IMAPMessage] = {
    val sortedIncrementing: Seq[IMAPMessage] =
      messages.sortBy(_.getReceivedDate)
    if (sortedIncrementing.length <= 1)
      return Seq()

    val badlyOrderedPairs: Iterator[Seq[IMAPMessage]] =
      sortedIncrementing.sliding(2).dropWhile(successiveMessages =>
        (
          folder.getUID(successiveMessages(0)) <
            folder.getUID(successiveMessages(1))
        )
        ||
        (
          successiveMessages(0).getReceivedDate ==
            successiveMessages(1).getReceivedDate)
        )
    val badlyOrderedWithDupes: ListBuffer[IMAPMessage] = new ListBuffer()
    for (pair <- badlyOrderedPairs)
      badlyOrderedWithDupes ++= pair.toList
    val badlyOrdered: Seq[IMAPMessage] =
      badlyOrderedWithDupes.distinct.toVector
    return badlyOrdered
  }
}
