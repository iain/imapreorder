// Copyright © 2014 Iain Nicol
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.iainnicol.imapreorder

import com.sun.mail.imap.IMAPFolder
import java.nio.file.{Files, Path}
import java.util.UUID
import javax.mail.Folder
import org.apache.commons.io.FileUtils
import resource.{ManagedResource, makeManagedResource}

/** Functions for temporary resources.
 */
object Tmp {

  /** A temporary directory resource, usable with the scala-arm library.
    * Immediately creates the concrete temporary directory.  Upon
    * release, deletes the concrete directory, ignoring errors.
    */
  def Dir(): ManagedResource[Path] = {
    val path: Path = Files.createTempDirectory(null)
    val resource = makeManagedResource(path)({ path =>
      FileUtils.deleteQuietly(path.toFile)
      () })(List())
    resource
  }

  /** A temporary IMAP folder resource, usable with the scala-arm library.
    * Immediately creates the concrete IMAP folder.  Upon release,
    * closes and deletes the concrete IMAP folder.
    *
    * @param parent parent folder to the temporary folder.
    */
  def ImapFolder(parent: IMAPFolder): ManagedResource[IMAPFolder] = {
    val uuid: UUID = UUID.randomUUID
    val folderName: String = s"imapreorder-tmp-$uuid"
    val folder: IMAPFolder =
      parent.getFolder(folderName).asInstanceOf[IMAPFolder]
    val isCreated: Boolean = folder.create(Folder.HOLDS_MESSAGES)
    if (!isCreated)
      throw new Exception("Unable to create temporary IMAP folder.")
    folder.open(Folder.READ_WRITE)
    val resource = makeManagedResource(folder)({ folder =>
      folder.close(false /* Expunge flag */)
      folder.delete(true /* Recurse flag */) })(List())
    resource
  }
}
