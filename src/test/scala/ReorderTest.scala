// Copyright © 2014 Iain Nicol
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import com.github.nscala_time.time.Imports
import com.iainnicol.imapreorder._

import com.github.nscala_time.time.Imports._
import com.sun.mail.imap.{IMAPFolder, IMAPStore, IMAPMessage}
import com.sun.security.auth.module.UnixSystem
import java.io.{File, FileWriter}
import java.nio.file.{Files, Path}
import java.util.UUID
import javax.mail.internet.{InternetAddress, MimeMessage}
import javax.mail.{Folder, Message, Session}
import org.apache.commons.io.FileUtils
import org.scalacheck.{Gen, Properties}
import org.scalacheck.Prop.forAll
import resource.managed
import scala.collection.immutable.Seq
import scala.sys.process.{Process, ProcessBuilder}

object ReorderTest extends Properties("Reorder") {

  private var initialized = false
  // Must be the first property in the object.
  property("setup") = forAll { a: Int =>
    if (!initialized)
      setup()
    initialized = true
    true
  }

  private val datetimes = List(
    new DateTime("2008-04-27T10:35:39"),
    new DateTime("2009-08-21T12:55:28"),
    new DateTime("2009-08-21T12:55:29"),
    new DateTime("2013-01-16T01:02:02"),
    new DateTime("2014-01-20T22:32:21"))
  private val genReceivedDate: Gen[List[DateTime]] =
    Gen.listOf(Gen.oneOf(datetimes))

  property("reordersImapFolder") = forAll(genReceivedDate) { receivedDates =>
    print(".") // Show progress because we're slow.

    var result: Boolean = false
    for (inbox <- Imap.getFolder(store, "Inbox", Folder.READ_WRITE)) {
      val folderToReorderName: String = UUID.randomUUID.toString
      val folderToReorder: IMAPFolder =
        inbox.getFolder(folderToReorderName).asInstanceOf[IMAPFolder]
      folderToReorder.create(Folder.HOLDS_MESSAGES)
      folderToReorder.open(Folder.READ_WRITE)

      folderToReorder.appendMessages(
        receivedDates.map(getDummyMessage(_, session)).toArray)
      val msgsBefore: Seq[IMAPMessage] =
        Imap.getMessages(folderToReorder).sortBy(folderToReorder.getUID(_))
      msgsBefore.zip(receivedDates).foreach({ x =>
        assert(new DateTime(x._1.getReceivedDate) == x._2) })

      Reorder.byInternalDate(
        store,
        folderToReorder.getFullName,
        true /* Expunge flag */)

      // Need to reopen the folder, because the underlying javamail API
      // is lame.  Otherwise we see previously-expunged messages, and
      // stale or invalid UIDs, when we getMessages.
      folderToReorder.close(false /* Expunge flag */)
      val folderToReorder2: IMAPFolder =
        inbox.getFolder(folderToReorderName).asInstanceOf[IMAPFolder]
      folderToReorder2.open(Folder.READ_WRITE)

      val msgsAfter: Seq[IMAPMessage] =
        Imap.getMessages(folderToReorder2).sortBy(folderToReorder2.getUID(_))
      result = isCorrectlyReordered(msgsBefore, msgsAfter)

      folderToReorder2.close(false /* Expunge flag */)
      folderToReorder2.delete(true /* Recurse flag */)
    }
    result
   }

  // Must be the last property in the object.
  property("cleanup") = forAll { a: Int =>
    if (initialized)
      cleanup()
    initialized = false
    true
  }

  /**
   * @param before assumed to be sorted incrementing by UID.
   * @param after ditto.
   */
  private def isCorrectlyReordered(
    before: Seq[IMAPMessage],
    after: Seq[IMAPMessage]): Boolean = {
    val areSameLength: Boolean = (before.length == after.length)

    val isIncrementingByReceivedDate: Boolean =
      after.sliding(2).filter(_.size == 2)
        .map(consecutiveMsgs => (consecutiveMsgs(0), consecutiveMsgs(1)))
        .map{case (m1, m2) =>
          (new DateTime(m1.getReceivedDate), new DateTime(m2.getReceivedDate))}
        .forall{case (d1, d2) => d1 <= d2}

    val areSameReceivedDates: Boolean =
      (before.map(_.getReceivedDate).sorted ==
        after.map(_.getReceivedDate).sorted)

    // TODO: Ideally we'd check whether each message, as identified by
    // its (unique) content, exists both before and after, and with the
    // same receivedDate.

    val result: Boolean = (
      areSameLength
        && isIncrementingByReceivedDate
        && areSameReceivedDates)
    result
  }

  private def getDummyMessage(date: DateTime, session: Session): Message = {
    val message: Message = new MimeMessage(session)

    // Can't set received date of a MimeMessage.  But if it's appended
    // to an IMAP Folder the received date will default to the sent
    // date.
    message.setSentDate(date.toDate)

    message.setFrom(new InternetAddress("sender@example.org"))
    message.setRecipient(
      Message.RecipientType.TO,
      new InternetAddress("recipient@example.org"))
    message.setSubject("TEST MESSAGE")

    message.setText("MESSAGE")

    message
  }

  var root: Path = null
  var session: Session = null
  var store: IMAPStore = null
  var imapServerProcess: Process = null

  private def setup(): Unit = {
    val port: Int = 10143
    val username: String = "imapreordertest"
    val password: String = UUID.randomUUID.toString

    root = Files.createTempDirectory(null)
    val dovecotConf: Path = configureDovecot(root, port, username, password)
    val imapServerProcessBuilder: ProcessBuilder = Process(
      "dovecot",
      List("-F", "-c", dovecotConf.toString))

    imapServerProcess = imapServerProcessBuilder.run()
    // This is ugly, but Dovecot won't be immediately available to connect to.
    Thread.sleep(5000)

    session = Session.getDefaultInstance(System.getProperties, null)
    store = Imap.getStore(session, Imap.Protocol.imap)
    store.connect("localhost", port, username, password)
  }

  private def cleanup(): Unit = {
    store.close()
    store = null
    session = null

    imapServerProcess.destroy()
    imapServerProcess = null

    FileUtils.deleteQuietly(root.toFile)
    root = null
  }

  private def configureDovecot(
    root: Path,
    port: Int,
    loginUsername: String,
    loginPassword: String): Path = {
    val etcDovecot: Path = root.resolve("etc/dovecot")
    val dovecotConf: Path = etcDovecot.resolve("dovecot.conf")
    val etcPasswd: Path = root.resolve("etc/passwd")

    etcDovecot.toFile.mkdirs()

    for (etcPasswdWriter <- managed(new FileWriter(etcPasswd.toFile))) {
      val etcPasswdContents: String = s"$loginUsername:{PLAIN}$loginPassword\n"
      etcPasswdWriter.write(etcPasswdContents)
    }

    for (dovecotConfWriter <- managed(new FileWriter(dovecotConf.toFile))) {
      val userInfo = new UnixSystem()
      val config: String = getDovecotDotConfContents(
        root,
        etcPasswd,
        port,
        userInfo.getUsername,
        userInfo.getUid,
        userInfo.getGid)
      dovecotConfWriter.write(config)
    }
    dovecotConf
  }

  private def getDovecotDotConfContents(
                root: Path, etcPasswd: Path,
                port: Int,
                filesystemUsername: String,
                filesystemUid: Long, filesystemGid: Long): String = {
    val homeDir: Path = root.resolve("home/%u").toAbsolutePath
    val config: String = s"""
        |protocols = imap
        |listen = localhost
        |base_dir = ${root.resolve("run/dovecot").toAbsolutePath}
        |instance_name = dovecot-test
        |
        |log_path = /dev/null
        |
        |ssl = no
        |
        |mail_location = maildir:~/Maildir
        |
        |default_internal_user = $filesystemUsername
        |default_login_user = $filesystemUsername
        |
        |service anvil {
        |  chroot =
        |}
        |
        |service imap-login {
        |  chroot =
        |    inet_listener imap {
        |      port = $port
        |    }
        |}
        |
        |userdb {
        |  args = uid=$filesystemUid gid=$filesystemGid home=$homeDir
        |  driver = static
        |}
        |
        |passdb {
        |  args = ${etcPasswd.toAbsolutePath}
        |  driver = passwd-file
        |}
        |"""

      config.stripMargin
  }
}
