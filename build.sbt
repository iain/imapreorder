// Copyright © 2014 Iain Nicol
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

name := "imapreorder"

version := "1.0.0"

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  "com.github.nscala-time" %% "nscala-time" % "1.2.0" % "test",
  "com.jsuereth" %% "scala-arm" % "1.4",
  "com.sun.mail" % "javax.mail" % "1.5.2",
  "javax.mail"	% "javax.mail-api" % "1.5.2",
  "org.apache.directory.studio" % "org.apache.commons.io" % "2.4",
  "org.rogach" %% "scallop" % "0.9.5",
  "org.scalacheck" %% "scalacheck" % "1.11.4" % "test"
)
